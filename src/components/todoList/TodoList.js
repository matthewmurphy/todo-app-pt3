import React, { Component } from "react";
import TodoItem from "../todoItem/TodoItem";
//import { connect } from 'react-redux'
//import { addTodo, toggleTodo, deleteTodo, clearCompletedTodos } from '/Users/matthewmurphy/demos/todo-pt-3/src/action.js';

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              title={todo.title}
              completed={todo.completed}
              id={todo.id}
              numOf={this.props.numOf}
            />
          ))}
        </ul>
      </section>
    );
  } 
}
// const mapStateToProps = (state) => {
//   return {
//     todos: state.todos,
//     itemsLeft: state.itemsLeft
//   }
// }
// const mapDispatchToProps = {
//   addTodo, toggleTodo, deleteTodo, clearCompletedTodos
// }


export default (TodoList);
//connect(mapStateToProps, mapDispatchToProps)
