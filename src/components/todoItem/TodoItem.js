import React, { Component } from 'react';
import { connect } from 'react-redux'
import { addTodo, toggleTodo, deleteTodo, clearCompletedTodos } from '../action';

class TodoItem extends Component {
  handleDelete = (id) => {
    this.props.deleteTodo(id)
  };
  handleComplete = (id) => {
    this.props.toggleTodo(id)
  };
    render() {
      return (
        <li className={this.props.todos.completed ? "completed" : ""}>
          <div className="view">
            <input
              className="toggle"
              type="checkbox"
              checked={this.props.completed}
              onClick={() => this.handleComplete(this.props.id)}
              onChange={(event) => this.props.numOf(event)}
            />
            <label>{this.props.title}</label>
            { 
              <button
                className="destroy"
                onClick={() => this.handleDelete(this.props.id)}
              />
            }
          </div>
        </li>
      );
    }
  }
  const mapStateToProps = (state) => {
    return {
      todos: state.todos,
      itemsLeft: state.itemsLeft
    }
  }
  const mapDispatchToProps = {
    addTodo, toggleTodo, deleteTodo, clearCompletedTodos
  }
  
  
  export default connect(mapStateToProps, mapDispatchToProps)(TodoItem);