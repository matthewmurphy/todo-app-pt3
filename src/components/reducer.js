import React from 'react';
import todosList from "../todos.json";
import {ADD_TODO} from './action';
import {TOGGLE_TODO} from './action';
import {DELETE_TODO} from './action';
import {CLEAR_COMPLETED_TODOS} from './action';
import {NUM_OF} from './action';


const intialState = {
    todos: todosList,
    itemsLeft: todosList.length
}

function todos (state = intialState, action) {
    switch (action.type) {
        case ADD_TODO:
            return Object.assign({}, state, {
                todos: [
                  ...state.todos,
                  {
                    userId: 1,
                    id: Math.ceil(Math.random() * 99999),
                    title: action.payload,
                    completed: false
                  }
                ]
              })
        case TOGGLE_TODO:
                let batman = [...state.todos];
                const clone = batman.map((todo) => {
                    if (todo.id === action.payload) {
                        todo.completed = !todo.completed;
                    }
                    return todo
            });
            return { todos: clone }
        case DELETE_TODO:
            let spiderman = [...state.todos];
            const oldClone = spiderman.filter((item) => item.id !== action.payload);
            return {todos: oldClone }
        case CLEAR_COMPLETED_TODOS:
            let megaman = [...state.todos];
            const newClone = megaman.filter((item) => item.completed !== true);
            return { todos: newClone };
        default: return state
    }    
}
export function itemsLeft (state = intialState, action) {
    switch(action.type) {
    case NUM_OF:
        let copy = [...state.itemsLeft];
        const cloner = copy.filter((item) => item.completed !== true);
        const clonerSize = cloner.length
        return clonerSize
    default: return state
    }
}
export default todos