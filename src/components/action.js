import React from 'react'

export const ADD_TODO = 'ADD_TODO';
export const TOGGLE_TODO = 'TOGGLE_TODO';
export const DELETE_TODO = 'DELETE_TODO';
export const CLEAR_COMPLETED_TODOS = 'CLEAR_DELETED_TODOS';
export const NUM_OF = 'NUM_OF'

export function addTodo (input) {
    return {
        type: ADD_TODO,
        payload: input
    }
}

export function toggleTodo (id) { 
    return {
        type: TOGGLE_TODO,
        payload: id
    }
}

export function deleteTodo (id) {
    return {
        type: DELETE_TODO,
        payload: id
    }
}

export function clearCompletedTodos () {
    return {
        type: CLEAR_COMPLETED_TODOS,
    }
}

export function numOf () {
    return {
        type:  NUM_OF
    }
}