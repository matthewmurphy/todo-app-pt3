import React, { Component } from "react";
import { connect } from 'react-redux'
import TodoList from "./components/todoList/TodoList";
import { Route, Switch, Link } from "react-router-dom";
import { addTodo, toggleTodo, deleteTodo, clearCompletedTodos, numOf } from './components/action';

class App extends Component {
  handleComplete = (id) => {
    this.props.toggleTodo(id)
  };
  addHandler = (event) => {
    if (event.key === "Enter") {
      this.props.addTodo(event.target.value)
    }
  };
  handleDelete = (id) => {
    this.props.deleteTodo(id)
  };
  handleClear = (event) => {
    this.props.clearCompletedTodos(event)
  };
  handleActive = (event) => {
    const clone = event.filter((item) => item.completed === true);
    return clone
  };
  numOf = () => {
    this.props.numOf()
  }
  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            type="text"
            placeholder="What needs to be done?"
            onKeyDown={this.addHandler}
            autoFocus
          />
        </header>
        {/* {<TodoList
          todos={this.state.todos}
          handleDelete={this.handleDelete}
          handleComplete={this.handleComplete}
          numOf={this.numOf}
        />} */}
         <Switch>
                <Route exact path="/" activeclassname='all' render={()=><TodoList 
                todos={this.props.todos} 
                numOf={this.numOf}/>}/>
                <Route exact path="/active" activeclassname='active' render={()=><TodoList
                todos={this.props.todos.filter((item) => item.completed !== true)}
                numOf={this.numOf}
                />} />
                <Route exact path="/completed" activeclassname='complete' render={()=><TodoList
                todos={this.props.todos.filter((item) => item.completed === true)}
                numOf={this.numOf}
                />} />
                <Route>
                    <p>404 ERROR</p>
                </Route>
        </Switch>  
        <footer className="footer">
          {/* <!-- This should be `0 items left` by default --> */}
          <span className="todo-count">
    <strong>{this.props.todos.filter((item) => item.completed === false).length}</strong> item(s) left
          </span>
          <ul className="filters">
            <li>
              <Link to="/" activeclassname='all'>All</Link>
            </li>
            <li>
              <Link to="/active" activeclassname='active'>Active</Link>
            </li>
            <li>
              <Link to="/completed" activeclassname='complete'>Completed</Link>
            </li>
          </ul> 
          <button className="clear-completed" onClick={() =>this.handleClear(this.props.todos)}>Clear completed</button>
        </footer>
      </section>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    todos: state.todos,
    itemsLeft: state.itemsLeft
  }
}
const mapDispatchToProps = {
  addTodo, toggleTodo, deleteTodo, clearCompletedTodos, numOf
}


export default connect(mapStateToProps, mapDispatchToProps)(App);

